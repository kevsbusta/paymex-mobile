export class RequestKey {
  constructor() { }

  mobile: number;
  first_name: string;
  last_name: string;
  description: string;
  granted_amount: number;
}
