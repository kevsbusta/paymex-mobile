export class Registration {
  constructor() { }
  first_name: string;
  last_name: string;
  mobile: number;
  email: string;
  password: string;
  birth_date: string;
}

export class UserLogin {
  mobile: string;
  password: string;
}
