export interface Activity {
  name: string;
  description: string;
  date: string;
  amount: number;
  days_remaining: number;
}

export class PendingActivity {
  constructor() {}

  receiver: string;
  createdAt: string
  description: string;
  currency: string;
  grantedAmount: string;
  status: string;
  daysRemaining: string;
  id: string;
}

export class UnconsumedActivity {
  constructor() {}

  receiver: string;
  createdAt: string
  description: string;
  currency: string;
  grantedAmount: string;
  status: string;
  daysRemaining: string;
  id: string;
}

export class ApproveActivity {
  constructor(){}
}

export class DenyActivity {
  constructor(){}
}
