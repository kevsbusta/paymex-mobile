import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { TokenInterceptor } from '../shared/interceptor';

// Pages
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RegistrationPage } from '../pages/registration/registration';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password';
import { OtpVerificationPage } from '../pages/otp-verification/otp-verification';
import { KeyRequestPage } from '../pages/key-request/key-request';
import { KeySendPage } from '../pages/key-send/key-send';
import { ActivitiesPage } from '../pages/activities/activities';
import { BillingSettingsPage } from '../pages/billing-settings/billing-settings';
import { PendingRequestPage } from '../pages/pending-request/pending-request';
import { ActivitiesSentPage } from '../pages/activities/activities-sent/activities-sent';
import { ActivitiesReceivedPage } from '../pages/activities/activities-received/activities-received';
import { UnconsumedKeysPage } from '../pages/unconsumed-keys/unconsumed-keys';
import { PaymexKeyServiceProvider } from '../providers/paymex-key-service/paymex-key-service';
import { UserService } from '../providers/registration-service/registration-service';
import { ActivityServiceProvider } from '../providers/activity-service/activity-service';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    RegistrationPage,
    ForgotPasswordPage,
    OtpVerificationPage,
    KeyRequestPage,
    ActivitiesPage,
    BillingSettingsPage,
    PendingRequestPage,
    KeySendPage,
    ActivitiesSentPage,
    ActivitiesReceivedPage,
    UnconsumedKeysPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    RegistrationPage,
    ForgotPasswordPage,
    OtpVerificationPage,
    KeyRequestPage,
    ActivitiesPage,
    BillingSettingsPage,
    PendingRequestPage,
    KeySendPage,
    ActivitiesSentPage,
    ActivitiesReceivedPage,
    UnconsumedKeysPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},

    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    PaymexKeyServiceProvider,
    UserService,
    ActivityServiceProvider
  ]
})
export class AppModule {}
