import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RequestOptions } from '@angular/http';
import { SendKey } from '../../shared/models/send-key';
import { RequestKey } from '../../shared/models/request-key';
import { Storage } from '@ionic/storage';
import { environment } from '../../app/constants';

/*
  Generated class for the PaymexKeyServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PaymexKeyServiceProvider {

  baseUrl: string;

  constructor(public http: HttpClient, private storage: Storage) {
    this.baseUrl = environment.baseUrl;
  }

  sendKey(data: SendKey) {
    return this.http.post(`${this.baseUrl}/send_paymex_keys`, data, {});
  }

  requestKey(data: RequestKey) {
    return this.http.post(`${this.baseUrl}/request_paymex_keys`, data, {});
  }
}
