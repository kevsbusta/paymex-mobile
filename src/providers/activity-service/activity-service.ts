import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../app/constants';
import { ApproveActivity, DenyActivity, PendingActivity } from '../../shared/models/activities';

/*
  Generated class for the ActivityServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ActivityServiceProvider {

  baseUrl: string;

  constructor(public http: HttpClient) {
    console.log('Hello ActivityServiceProvider Provider');
    this.baseUrl = environment.baseUrl;
  }

  approveKey(paymex_id: any) {
    return this.http.post(`${this.baseUrl}/paymex_keys/${paymex_id}/approve`, {}, {});
  }

  denyKey(paymex_id: any) {
    return this.http.post(`${this.baseUrl}/paymex_keys/${paymex_id}/deny`, {}, {});
  }

  cancelKey(paymex_id: any) {
    return this.http.post(`${this.baseUrl}/paymex_keys/${paymex_id}/cancel`, {}, {});
  }

  getAllPending() {
    return this.http.get(`${this.baseUrl}/pending_paymex_keys`);
  }

  getUnconsumed() {
    return this.http.get(`${this.baseUrl}/unconsumed_paymex_keys`);
  }
}
