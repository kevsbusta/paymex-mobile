import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Registration } from '../../shared/models/user';
import { environment } from '../../app/constants';

/*
  Generated class for the UserService provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserService {

  baseUrl: string;

  constructor(public http: HttpClient) {
    console.log('Hello UserService Provider');
    this.baseUrl = environment.baseUrl;
  }

  register(data: Registration) {
    return this.http.post(`${this.baseUrl}/users`, data, {});
  }

  login(data) {
    return this.http.post(`${this.baseUrl}/user_authentication/authenticate`, data, {});
  }
}
