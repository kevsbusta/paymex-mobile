import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KeyRequestPage } from './key-request';

@NgModule({
  declarations: [
    KeyRequestPage,
  ],
  imports: [
    IonicPageModule.forChild(KeyRequestPage),
  ],
})
export class KeyRequestPageModule {}
