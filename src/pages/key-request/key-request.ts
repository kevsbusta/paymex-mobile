import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PaymexKeyServiceProvider } from '../../providers/paymex-key-service/paymex-key-service';
import { RequestKey } from '../../shared/models/request-key';

/**
 * Generated class for the KeyRequestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-key-request',
  templateUrl: 'key-request.html',
})
export class KeyRequestPage {
  data: RequestKey = new RequestKey();

  constructor(public navCtrl: NavController, public navParams: NavParams, public service: PaymexKeyServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad KeyRequestPage');
  }

  requestKey() {
    console.log(this.data);
    this.service.requestKey(this.data)
      .subscribe(
        res => {
        console.log(res);
      },
        err => {
        console.error(err);
      }
    )
  }

}
