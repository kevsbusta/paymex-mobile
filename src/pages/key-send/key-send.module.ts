import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KeySendPage } from './key-send';

@NgModule({
  declarations: [
    KeySendPage,
  ],
  imports: [
    IonicPageModule.forChild(KeySendPage),
  ],
})
export class KeySendPageModule {}
