import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SendKey } from '../../shared/models/send-key';
import { PaymexKeyServiceProvider } from '../../providers/paymex-key-service/paymex-key-service';

/**
 * Generated class for the KeySendPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-key-send',
  templateUrl: 'key-send.html',
})
export class KeySendPage {
  data: SendKey = new SendKey();

  constructor(public navCtrl: NavController, public navParams: NavParams, public service: PaymexKeyServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad KeySendPage');
  }

  sendKey() {
    console.log(this.data);
    this.service.sendKey(this.data)
    .subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.error(err);
      }
    )
  }
}
