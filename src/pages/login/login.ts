import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { RegistrationPage } from '../registration/registration';
import { ForgotPasswordPage } from '../forgot-password/forgot-password';
import { UserService } from '../../providers/registration-service/registration-service';
import { UserLogin } from '../../shared/models/user';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  data: UserLogin = new UserLogin();

  constructor(public navCtrl: NavController, public navParams: NavParams, public service: UserService,
              private storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login() {
    console.log(this.data);
    this.service.login(this.data)
    .subscribe(
      (res:any) => {
        console.log(res.auth_token);
        this.storage.set('token', res.auth_token);
        this.navCtrl.setRoot(HomePage);
      },
      (err) => {
        console.error(err);
      }
    );
  }

  register() {
    this.navCtrl.push(RegistrationPage);
  }

  forgotPassword() {
    this.navCtrl.push(ForgotPasswordPage);
  }

}
