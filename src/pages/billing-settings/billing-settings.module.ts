import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BillingSettingsPage } from './billing-settings';

@NgModule({
  declarations: [
    BillingSettingsPage,
  ],
  imports: [
    IonicPageModule.forChild(BillingSettingsPage),
  ],
})
export class BillingSettingsPageModule {}
