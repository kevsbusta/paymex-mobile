import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { OtpVerificationPage } from '../otp-verification/otp-verification';
import { UserService } from '../../providers/registration-service/registration-service';
import { Registration } from '../../shared/models/user';

/**
 * Generated class for the RegistrationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
})
export class RegistrationPage {

  registration: Registration = new Registration();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: UserService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrationPage');
  }

  register() {
    console.log(this.registration);
    this.service.register(this.registration)
    .subscribe(
      res => {
        console.log(res);
        this.navCtrl.push(OtpVerificationPage);
      },
      err => {
        console.error(err);
      }
    )
  }
}
