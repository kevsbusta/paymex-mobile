import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { UnconsumedActivity } from '../../shared/models/activities';
import { ActivityServiceProvider } from '../../providers/activity-service/activity-service';

/**
 * Generated class for the UnconsumedKeysPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-unconsumed-keys',
  templateUrl: 'unconsumed-keys.html',
})
export class UnconsumedKeysPage {
  activities: UnconsumedActivity[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public alerCtrl: AlertController,
  public service: ActivityServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UnconsumedKeysPage');
    this.service.getUnconsumed()
      .subscribe(
      (res:any) => {
        console.log(res.data);
        this.activities = res.data;
      },
        err => {
        console.error(err);
      }
    );
    //this.activities = [
    //  {name: "John Doe", date: "December 1, 2017", amount: 4999, days_remaining: 3, description: "Your description here Your description here Your description here"},
    //  {name: "Jean Dae", date: "December 1, 2017", amount: 3999, days_remaining: 2, description: "Your description here Your description here Your description here"},
    //  {name: "Jose Mario", date: "December 1, 2017", amount: 1500, days_remaining: 7, description: "Your description here Your description here Your description here"},
    //];
  }

  itemClick(activity: UnconsumedActivity) {
    let confirm = this.alerCtrl.create({
      title: 'Unconsumed Key',
      message: `
        <h3>` + activity.receiver + `</h3>
        <h5>` + activity.createdAt + `</h5>
        <p>` + activity.description + `</p>

        <br>

        <h3>P` + activity.grantedAmount + `</h3>
        <h5>Expires in ` + activity.daysRemaining + ` days</h5>`,
      buttons: [
        {
          text: 'Close',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Remove Key',
          handler: () => {
            this.service.cancelKey(activity.id)
              .subscribe(
                res => {
                console.log(res);
                this.navCtrl.pop();
              },
                err => {
                console.error(err);
              }
            );
          }
        }
      ]
    });
    confirm.present()
  }

}
