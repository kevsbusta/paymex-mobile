import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UnconsumedKeysPage } from './unconsumed-keys';

@NgModule({
  declarations: [
    UnconsumedKeysPage,
  ],
  imports: [
    IonicPageModule.forChild(UnconsumedKeysPage),
  ],
})
export class UnconsumedKeysPageModule {}
