import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { KeyRequestPage } from '../key-request/key-request';
import { KeySendPage } from '../key-send/key-send';
import { ActivitiesPage } from '../activities/activities';
import { BillingSettingsPage } from '../billing-settings/billing-settings';
import { PendingRequestPage } from '../pending-request/pending-request';
import { UnconsumedKeysPage } from '../unconsumed-keys/unconsumed-keys';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  pages = [
    { "key": 1, "page": KeySendPage},
    { "key": 2, "page": KeyRequestPage},
    { "key": 3, "page": ActivitiesPage},
    { "key": 4, "page": BillingSettingsPage},
    { "key": 5, "page": PendingRequestPage},
    { "key": 6, "page": UnconsumedKeysPage},
  ];

  constructor(public navCtrl: NavController) { }

  navigate(page: number) {
    this.navCtrl.push(this.pages.find(x => x.key == page).page);
  }

}
