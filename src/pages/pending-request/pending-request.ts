import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { PendingActivity } from '../../shared/models/activities';
import { ActivityServiceProvider } from '../../providers/activity-service/activity-service';

/**
 * Generated class for the PendingRequestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pending-request',
  templateUrl: 'pending-request.html',
})
export class PendingRequestPage {
  activities: PendingActivity[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public alerCtrl: AlertController, public service: ActivityServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PendingRequestPage');
    this.service.getAllPending()
    .subscribe(
      (res:any) => {
        console.log(res.data);
        this.activities = res.data;
      },
      err => {
        console.error(err);
      }
    );
  }

  itemClick(activity: PendingActivity) {
    let confirm = this.alerCtrl.create({
      title: 'Pending Request',
      message: `
        <h3>` + activity.receiver + `</h3>
        <h5>` + activity.createdAt + `</h5>
        <p>` + activity.description + `</p>

        <br>

        <h3>`+ activity.currency + activity.grantedAmount + `</h3>
        <h5>Expires in ` + activity.daysRemaining + ` days</h5>`,
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            this.service.cancelKey(activity.id)
            .subscribe(
              res => {
                console.log(res);
                this.navCtrl.pop();
              },
              err => {
                console.error(err);
              }
            );
          }
        },
        {
          text: 'Decline',
          handler: () => {
            this.service.denyKey(activity.id)
            .subscribe(
              res => {
                console.log(res);
                this.navCtrl.pop();
              },
              err => {
                console.error(err);
              }
            );
          }
        },
        {
          text: 'Accept',
          handler: () => {
            this.service.approveKey(activity.id)
              .subscribe(
                res => {
                console.log(res);
                  this.navCtrl.pop();
              },
                err => {
                console.error(err);
              }
            );
          }
        }
      ]
    });
    confirm.present()
  }

  test(){

    this.navCtrl.pop();
  }

}
