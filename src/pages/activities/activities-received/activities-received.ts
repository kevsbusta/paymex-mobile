import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Activity } from '../../../shared/models/activities';

/**
 * Generated class for the ActivitiesReceivedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-activities-received',
  templateUrl: 'activities-received.html',
})
export class ActivitiesReceivedPage {
  activities: Activity[];
  filter: any = "all";

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActivitiesReceivedPage');
    this.activities = [
      {name: "John Doe", date: "December 1, 2017", amount: 1200, description: "", days_remaining: null},
      {name: "Jean Dae", date: "December 1, 2017", amount: 250, description: "", days_remaining: null},
      {name: "Jose Mario", date: "December 1, 2017", amount: 500, description: "", days_remaining: null},
    ];
  }

}
