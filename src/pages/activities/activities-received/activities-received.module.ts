import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActivitiesReceivedPage } from './activities-received';

@NgModule({
  declarations: [
    ActivitiesReceivedPage,
  ],
  imports: [
    IonicPageModule.forChild(ActivitiesReceivedPage),
  ],
})
export class ActivitiesReceivedPageModule {}
