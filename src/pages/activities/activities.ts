import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ActivitiesSentPage } from './activities-sent/activities-sent';
import { ActivitiesReceivedPage } from './activities-received/activities-received';

/**
 * Generated class for the ActivitiesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-activities',
  templateUrl: 'activities.html',
})
export class ActivitiesPage {
  sentTab: any;
  receivedTab: any;

  constructor(public navCtrl: NavController, public params: NavParams) {
    this.sentTab = ActivitiesSentPage;
    this.receivedTab = ActivitiesReceivedPage;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActivitiesPage');
  }



}
