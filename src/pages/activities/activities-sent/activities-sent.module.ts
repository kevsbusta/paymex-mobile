import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActivitiesSentPage } from './activities-sent';

@NgModule({
  declarations: [
    ActivitiesSentPage,
  ],
  imports: [
    IonicPageModule.forChild(ActivitiesSentPage),
  ],
})
export class ActivitiesSentPageModule {}
