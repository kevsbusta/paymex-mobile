import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Activity } from '../../../shared/models/activities';

/**
 * Generated class for the ActivitiesSentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-activities-sent',
  templateUrl: 'activities-sent.html',
})
export class ActivitiesSentPage {
  activities: Activity[];
  filter: any = "all";

  constructor(public navCtrl: NavController, public params: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActivitiesSentPage');
    this.activities = [
      {name: "John Doe", date: "December 1, 2017", amount: 4999, description: "", days_remaining: null},
      {name: "Jean Dae", date: "December 1, 2017", amount: 3999, description: "", days_remaining: null},
      {name: "Jose Mario", date: "December 1, 2017", amount: 1500, description: "", days_remaining: null},
    ];
  }

}
